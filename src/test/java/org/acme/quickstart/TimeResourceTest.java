package org.acme.quickstart;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class TimeResourceTest {

    @Test
    public void testTime(){

        given().
                when().get("/time_Resource/time")
                .then()
                .statusCode(200)
                .body(is(LocalDate.now().toString()));
    }

}
