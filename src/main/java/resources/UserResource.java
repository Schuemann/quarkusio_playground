package resources;

import entities.User;
import persistence.DatabaseConnector;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/user")
public class UserResource {


    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/firstUser")
    public String hello() {

            List<User> u =  DatabaseConnector.getConnectionFactory().createNamedQuery("UserQuery.findAll").getResultList();

            if(u == null){
                return "shit";
            } else {
                return u.get(0).getName();
            }

    }

}
