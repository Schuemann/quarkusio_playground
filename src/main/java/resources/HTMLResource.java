package resources;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;

@Path("/someHtml")
public class HTMLResource {

    // TODO: Display index.html as first Website with self defined URL. ->  Not working yet!
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("simpleBody")
    public String rootResource() throws IOException {

        Document doc = Jsoup.parse("<html></html>");
        doc.body().addClass("body-styles-cls");
        doc.body().appendElement("div").append("<p1>Hello World!</p1>");

        return doc.toString();
    }




}
