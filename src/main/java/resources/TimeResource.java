package resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.time.LocalDate;

@Path("/time_Resource")
public class TimeResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/client_time")
    public LocalDate getClientTime(){
        return LocalDate.now();
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/dayOfMonth")
    public Integer getDaysOfMonth(){
        return LocalDate.now().getDayOfMonth();
    }

}
