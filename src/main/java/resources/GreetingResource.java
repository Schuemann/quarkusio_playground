package resources;

import jdk.nashorn.internal.objects.annotations.Getter;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Path("/hello")
public class GreetingResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/simpleHello")
    public CompletionStage<String> hello() {
        return CompletableFuture.supplyAsync(() -> { // For Future
            return "hello";
        });
    }

    @GET
    @Produces({MediaType.TEXT_PLAIN})
    @Path("/greeting/{name}")
    public String greeting(@PathParam("name") String name){
        return "hello " + name;
    }

}