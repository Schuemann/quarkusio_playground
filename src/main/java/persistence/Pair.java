package persistence;

import java.io.Serializable;

public class Pair<A,B> implements Serializable
{
    private static final long serialVersionUID = -1985113215389758610L;

    private final A _first;
    private final B _second;

    public Pair(final A first, final B second)
    {
        _first = first;
        _second = second;
    }


    public A getFirst() {
        return _first;
    }


    public B getSecond() {
        return _second;
    }

}