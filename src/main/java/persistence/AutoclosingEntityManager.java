package persistence;

import javax.persistence.EntityManager;
import java.io.Closeable;

public interface AutoclosingEntityManager extends EntityManager, Closeable {
}
