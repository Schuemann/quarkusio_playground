package persistence;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class DatabaseConnector {

    public static AutoclosingEntityManager getConnectionFactory(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("PersistenceUnit");
        EntityManager em = emf.createEntityManager();
        AutoclosingEntityManager cem = wrap(em);
        return cem;
    }

    private static final Map<Long, Stack<Pair<AutoclosingEntityManager, AtomicInteger>>> emMap = new ConcurrentHashMap<>();

    private static AutoclosingEntityManager wrap(EntityManager em) {
        EntityManager old = em;
        return (AutoclosingEntityManager) Proxy.newProxyInstance(
                AutoclosingEntityManager.class.getClassLoader(),
                new Class[]{AutoclosingEntityManager.class},
                (proxy, method, args) -> {
                    if (method.getName().equals("close")) {
                        close(old);
                        return null;
                    }
                    return method.invoke(old, args);
                });
    }

    private static void close(EntityManager em) {
        Stack<Pair<AutoclosingEntityManager, AtomicInteger>> s = emMap.get(Thread.currentThread().getId());
        Pair<AutoclosingEntityManager, AtomicInteger> p = s.peek();
        if (p.getSecond().decrementAndGet() == 0) {
            try {
                s.pop();
                EntityTransaction tr = em.getTransaction();
                if (tr.isActive()) {
                    if (tr.getRollbackOnly()) {
                        tr.rollback();
                    } else {
                        tr.commit();
                    }
                }
            } finally {
                em.close();
            }
        }
    }

}
