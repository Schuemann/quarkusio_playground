package entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "\"USER\"", schema = "\"quarkus_playground\"")
@NamedQueries({
        @NamedQuery(name = "UserQuery.findAll", query = "SELECT f FROM User f"),
        @NamedQuery(name = "UserQuery.countFindAll", query = "SELECT COUNT(f.id) FROM User f"),
        @NamedQuery(name = "UserQuery.findUserByID", query = "SELECT f FROM User f WHERE f.id = :id ORDER BY f.name")
})
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "\"ID\"", nullable = false, length = 2147483647)
    String id;

    @Column(name = "\"NAME\"")
    String name;

    @Column(name = "\"SURNAME\"")
    String surname;

    @Column(name = "\"AGE\"")
    Integer age;


    public User() {
    }

    public User(String name, String surname, Integer age, Double height) {
        this.name = name;
        this.surname = surname;
        this.age = age;
      }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

}
